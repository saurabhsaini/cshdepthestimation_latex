% Template for ICIP-2015 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx}

\def\x{{\mathbf x}}
\def\L{{\cal L}}

\title{Unsupervised Monocular image structure estimation by CSH and Domain
Adaptation}
\twoauthors
{Saurabh Saini}
    {CVIT\\
    IIIT\\
    Hyderabad}
{P.J. Narayanan}
    {CVIT\\
    IIIT\\
    Hyderabad}

\begin{document}
\maketitle
\begin{abstract}
In this paper, we attempt to estimate the structure of a scene from a single 
image. For this we propose a data-driven framework by formulating a 
non-parametric optimization problem. For a given query image we retrieve a set 
of similar images from a pre-defined dataset of RGBD images (NYU v2 dataset). We 
use the patches from these retrieved candidate images to compute an Approximate 
Nearest Neighbour Field (ANNF). Shape features are calculated from these patches 
in the form of surface normals, normal gradients, plane/edge convexity 
indicators and occlusion cues. Feature extraction is done twice, first by using 
depth data and then  without it. Features computed using depth data are then 
clustered using Coherency Sensitive Hashing (CSH). These cluster labels are then 
forced on the without-depth features. Now in order to capture the shift between 
these two feature distributions we compute feature space transformation matrix 
for each cluster by employing a (semi-)supervised domain adaptation technique. 
Finally this transformation matrix is used while estimating query image 
structure by formulating an energy minimization model.
\end{abstract}

\begin{keywords}
Monocular depth estimation, Approximate nearest neighbour field, Coherency 
Sensitive Hashing, domain adaptation
\end{keywords}

\section{Introduction}
\label{sec:intro}
Scene structure estimation from a single image is a crucial and classic problem 
in computer vision. The problem requires fundamental image understanding in the 
form of both basic geometric reasoning and learnt pattern behaviours. 
%<< EXPLAIN ? >>%
We humans have the capability to estimate the structure of a scene by 
guessing the relative depths, occlusion boundaries and surface normals of 
various layers composing a given scene even in a monocular setting. This can be 
illustrated by noticing that an observer can have a very good general idea about 
the relative depths of objects in a scene just by looking at its 2D image. This 
can be attributed to the observer's reasoning accumulated gradually over a 
period of time. This makes \textit{monocular scene-structure estimation} a 
fundamentally different problem than stereopsis. Proposed solutions to the 
former problem generally rely heavily on statistical learning and pattern 
matching, while the solutions to the latter are akin to the techniques based on 
scene-geometry heuristics. In our work we try to incorporate both the 
approaches by deriving shape cues from the scene using geometric heuristics and 
then employing statistical learning on top of it for estimating the latent 
structural information.

In this paper we present a novel attempt at a solution to the aforementioned  
problem \textit{i.e. Estimating structure of a scene, provided with only 
a single monocular image}. Specifically, we try to estimate surface normals of 
various objects composing a given scene from its image. For this, we propose a 
data-driven non-parametric framework. We utilize readily available depth 
datatsets, captured using inexpensive and commercially available sensors like 
Microsoft Kinect. We use NYU depth dataset v2, introduced in [], for our 
experiments.

Intensive data-driven techniques have recently captured a lot of attention from 
the computer vision community. This can be attributed mainly to the continuously 
increasing prowess of the computing systems and profusion of data in this 
field. Intensive data-driven techniques have received a further boost with the 
advent of novel methods like deep learning. Data-driven techniques have 
been around for a while now for retrieval and recognition problems. This can be 
observed from the increasing use of large datasets like ImageNet[], PASCAL[], 
SUN[] etc. in contemporary research works.
%<< REFERENCE : ImageNet, PASCAL-VOC based papers. >>% 
For reconstruction problems also data-driven techniques are now being employed
extensively. This is particularly true in the case of Structure-from-motion 
(SfM) based methods like [,,]. All of these use publicly available large image 
collections for constructing a point cloud for a known monument or a city. 
Apart from this massive datasets have been used for single image reconstruction 
also. Such problems are generally posed as hole-filling or scene completion 
problems \textit{e.g.} Efros et. al. use 10 million small images for scene 
completion in []. 
%<< Give one more example here >>%

For monocular scene-structure estimation the research literature can be crudely 
classified in two sets. First is the set of methods which try to model the 
process using probabilistic graphical models like MRFs, Bayesian nets etc. 
wherein the focus is on building a generative model and learning the parameters 
. This is the approach by Saxena et. al. in [] and its related papers where they 
use specifically constructed features and MRFs to estimate depths for the 
outdoor images. This same approach was improved upon by Grumann et. al. in [] 
where they incorporate semantic information to improve upon the results of []. 
All these approaches are restricted by the assumptions in the feature  
construction and/or the modeling steps. Lately researchers have also started 
using large datasets for depth estimation of monocular images directly. This 
forms the second set of methods \textit{e.g.} Konard et. al.[.], Kevin et. 
al.[,], Herbert et. al.[] and Liu et. al.[]. Unlike the parametric methods 
discussed above, main focus in all of these papers is on a building 
a generalized error minimization framework. All these papers follow a similar 
core pipeline to achieve their objective. We have also taken inspiration from 
these methods and utilized the effectiveness of the mentioned pipeline to our 
end. One major difference in our approach being that we use Coherency Sensitive 
Hashing (CSH) for computing Approximate Nearest Neighbours Field (ANNF) of the 
image patches. This will be explained further in the section [].

Another objective of our research work is to experiment with the effects of 
formulating the monocular image scene-structure estimation problem in a domain 
adaptive setting. For this we extract features by two different methods :  
Method one does not use the captured depth information of the scene and relies 
solely on the intensity information to capture the underlying geometry of the 
scene (depth, surface normals, occlusion etc.). We call this method 
\textit{without-depth method (wo-Method)}. This defines the \textit{source 
domain} of our data. Method two on the other hand uses the entire RGBD data. 
This gives a relatively accurate estimate of the underlying geometry. We take 
this to be the ground truth for our work. We call this method 
\textit{with-depth method (w-Method)}. This defines the \textit{target domain} 
of our data. Now in order to minimize the effect of distribution shift between 
these two domains, we learn a transformation between the associated two feature 
spaces. This is done my formulating a constrained convex optimization problem. 
This will be explained further in the section[].

The organization of the paper is as follows : First we discuss the background 
and related  work in section []. Then we introduce our proposed method in 
section[]. We discuss our feature computation strategy in section[]. In 
section[] we discuss the CSH based ANNF estimation technique for these 
features. After that in section[] we cover how we use domain adaptation 
technique to compute feature space transformation matrix[ces]. Then in 
section[] we provide our experiment details and results. Finally in section[] 
we provide a learning summary and conclusion. 

\section{Background and related work}
\label{sec:background and related work}
Data-driven non-parametric approach for automatic depth estimation was 
initially introduced by Konard et. al. in [] and improved in []. In [] they use 
sift-flow[] between two images for establishing correspondence between the two 
images which being an computationally expensive was later on removed in their 
future work []. They focus on automatic 2D-to-3D conversion in images 
(extendable to videos) via two techniques : local and global. Their local 
technique is a point-to-point mapping function learnt using nearest-neighbour 
regression between intensity and depth values. For a given training set of 
image-depth pairs, they learn separate functions using color (hue and 
intensity), location (pixel x,y position) and motion (optical flow) values at a 
given pixel to its corresponding depth. All these values are then linearly 
combined to give the final depth value at that pixel. They use averaging of same 
color value pixels in the training set as their color-mapping function. For 
location-mapping, they take average of all depth values at that pixel location 
for all the training images. For motion-mapping, they compute optical flow and 
use Cross-Bilateral Filtering[] for tackling noisy masks. For the global method 
they follow the following steps : First they compute $K$ nearest 
neighbours of the query image using HOG[] features. Second they compute a 
global depth estimate of the scene using median filtering at each pixel 
location. Then they perform Cross Bilateral Filtering based on query image to 
get proper edge boundaries in the estimated depth. Although their method is 
simpler in modeling complexity yet it delivers comparable results to the 
state-of-the-art methods in the field.

Another noticeable work on this problem is by Kevin et. al. They introduce a 
depth estimation pipeline quite similar to []. In their approach after 
computing $K$ nearest neighbours of the given image, they compute sift flow 
between them. Then using the pixel correspondences obtained between thus, they 
build energy terms based on warped sift differences and intensity differences 
in values and in gradients. They also use motion segmentation and optical flows 
to extend their work to videos. They solve this entire energy minimization 
framework using iteratively re-weighted least squares minimization.
%<< Expand this section>>%

Recently, Liu et. al. []  also have used similar data-driven approach for 
estimating the depth of a scene. They


\section{PAGE TITLE SECTION}
\label{sec:pagestyle}



%\bibliographystyle{IEEEbib}
%\bibliography{refs}

\end{document}
